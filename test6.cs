using System;

class Program
{
    static void Main(string[] args)
    {
        try{
            String result = "a" ?? (int?)1;

            if (result == 1)

                Console.WriteLine("pass");

            else

                Console.WriteLine("fail");
            }
        catch(Exception e)
        {
            Console.WriteLine("Error, incompatible nullable types");
        }
    }
}