using System;

class Program
{
    static void Main(string[] args)
    {
        String result = "a" ?? null;

        if (result == "a")

            Console.WriteLine("pass");

        else

            Console.WriteLine("fail");

    }
}