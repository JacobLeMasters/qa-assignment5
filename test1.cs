using System;

class Program
{
    static void Main(string[] args)
    {
        int? result = (int?)1 ?? null;

        if (result == 1)

            Console.WriteLine("pass");

        else

            Console.WriteLine("fail");

    }
}